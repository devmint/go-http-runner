module gitlab.com/devmint/go-http-runner

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/stretchr/testify v1.6.1
	gitlab.com/devmint/go-restful v0.0.0-20210217205511-2c2d722a39da
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
