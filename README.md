<div align="center">
    <img src="./logo.png" alt="Go HTTP Runner" />
</div>

<div align="center">
    <strong>Simple utility to quickly write HTTP tests with internal HTTP server. It's developed to test E2E endpoints created in application.</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/Lang-GO-%2329BEB0?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/devmint/go-http-runner/master?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/coverage/devmint/go-http-runner/master?style=for-the-badge" />
</div>

<br />
<br />

## Example

#### Endpoint returns JSON error message

```go
func Test_RandomEndpoint(t *testing.T) {
	runner := EndToEndTestRunner{
		Server: exampleServer(),
	}

	runner.GET(t, "/abcd", Expectations{
		Name:       "invalid endpoint",
		StatusCode: http.StatusBadRequest,
		Body: `{
			"detail": "invalid endpoint",
			"title": "Bad Request",
			"type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
			"status": 400
		}`,
		Headers: map[string]string{
			"Content-type": "application/json",
		},
	})
}
```

#### Endpoint returns XML error message

```go
func Test_RandomEndpoint(t *testing.T) {
	runner := EndToEndTestRunner{
		Server: exampleServer(),
	}

	runner.GET(t, "/abcd", Expectations{
		StatusCode: http.StatusBadRequest,
		Body: `<response>
			<type>http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html</type>
			<title>Bad Request</title>
			<detail>invalid endpoint</detail>
			<status>400</status>
		</response>`,
		BodyContains: []string{
			`<title>Bad Request</title>`,
			`<detail>invalid endpoint</detail>`,
		},
		Headers: map[string]string{
			"Content-type":   "application/xml",
			"Dolor-sit-amet": "xkcd",
		},
		Before: func(r *http.Request) {
			r.Header.Set("Content-type", "application/xml")
		},
	})
}
```